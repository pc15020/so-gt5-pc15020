#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#define MODE 0666
#define BUF_SIZE 256
void main(){
    int src, dst, in, out;
    char buf[BUF_SIZE];
    
    src = open("fileA.txt", O_RDONLY);
    if (src < 0)
        exit(2);
    dst = creat("fileB.txt", MODE);
    if (dst < 0)
        exit(3);
        
    while (1){
        in = read(src, buf, BUF_SIZE);
        if (in <= 0)
            break;
        out = write(dst, buf, in);
        if (out <= 0)
            break;
    }
    
    close(src);
    close(dst);
    
    exit(0);
}