#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

#define BUF_SIZE 256

int main(void){

    char entrada[31];

    syscall(SYS_write, 1, "Ingrese Texto (MAX 30 Bytes): ", 30);
    int nread = read(0, entrada, sizeof entrada -1);
    entrada[nread-1] = '\0'; // reste 1 para sobrescribir la nueva línea

    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    
    
    return 0;
}