#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

#define BUF_SIZE 256

int main(void){

    int src, in;
    char buf[BUF_SIZE];

    src = open("fileA.txt", O_RDONLY);

    if (src < 0){
        syscall(SYS_write, 1, "Error Con el archivo", 20);
    }

    while (1){
        
        in = read(src, buf, BUF_SIZE);
        if (in <= 0){
            break;
        }

        syscall(SYS_write, 1, buf, strlen(buf));
    }
    
    close(src);
    
    return 0;
}